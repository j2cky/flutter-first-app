import 'package:flutter/material.dart';
import 'package:webapp/entity/post.dart';
import 'package:webapp/service/webservice.dart';

class DetailPost extends StatefulWidget {
    final int id;

    const DetailPost({Key key, @required this.id}) : super(key: key);

    @override
    State<StatefulWidget> createState() => _DetailPost();
}

class _DetailPost extends State<DetailPost> {
    Post _post = Post();

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(
                title: Text('Post'),
            ),
            body: Column(
                children: <Widget>[
                    Text(_post.title ?? 'Loading'),
                    Text(_post.content ?? '...'),
                ],
            ),
        );
    }

    @override
    void initState() {
        super.initState();
        _fetchPost();
    }

    void _fetchPost() {
        Webservice().load(Post.find(widget.id)).then((value) {
            setState(() {
                _post = value;
            });
        });
    }
}