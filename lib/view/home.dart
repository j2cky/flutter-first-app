import 'package:flutter/material.dart';
import 'package:webapp/entity/post.dart';
import 'package:webapp/service/webservice.dart';
import 'package:webapp/view/detail-post.dart';
import 'package:webapp/view/favorite.dart';

class HomePage extends StatefulWidget {
    HomePage({Key key, this.title}) : super(key: key);

    final String title;

    @override
    _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
    List<Post> _list = List<Post>();
    final Set<Post> _saved = Set<Post>();
    final _biggerFont = const TextStyle(fontSize: 18.0);

    @override
    void initState() {
        super.initState();
        _fetchPost();
    }

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(
                title: Text(widget.title),
                actions: <Widget>[
                    IconButton(icon: Icon(Icons.list), onPressed: () => {
                        //Navigator.pushNamed(context, '/favorite')
                        Navigator.push(context, MaterialPageRoute(builder: (context) => FavoritePage(list: _saved,)))
                    })
                ],
            ),
            body: _buildList(),
        );
    }

    Widget _buildList() {
        return ListView.builder(
            itemBuilder: (context, idx) {
                return _buildRow(context, idx);
            },
            itemCount: _list.length,
        );
    }

    Widget _buildRow(BuildContext context, int idx) {
        final bool alreadySaved = _saved.contains(_list[idx]);
        return ListTile(
            title: Text(
                _list[idx].title,
                style: _biggerFont,
            ),
            trailing: Icon(
                alreadySaved ? Icons.favorite : Icons.favorite_border,
                color: alreadySaved ? Colors.red : null,
            ),
            onTap: () {
                setState(() {
                    if (alreadySaved) {
                        _saved.remove(_list[idx]);
                    } else {
                        _saved.add(_list[idx]);
                    }
                });
                Navigator.push(context, MaterialPageRoute(builder: (context) => DetailPost(id: _list[idx].id)));
            },
        );
    }

    void _fetchPost() {
        Webservice().load(Post.all).then((value) => {
            setState(() => {
                _list  = value
            })
        });
    }
}