import 'package:flutter/material.dart';
import 'package:webapp/entity/post.dart';

class FavoritePage extends StatefulWidget {
    final Set<Post> list;

    FavoritePage({Key key, @required this.list}): super(key: key);

    @override
    State<StatefulWidget> createState() => _FavoritePage();
}

class _FavoritePage extends State<FavoritePage> {
    @override
    Widget build(BuildContext context) {
        final Iterable<ListTile> tiles = widget.list.map((e) => ListTile(
            title: Text(e.title),
        ));
        final List<Widget> divided = ListTile.divideTiles(
            context: context,
            tiles: tiles,
        ).toList();
        return Scaffold(
            appBar: AppBar(
                title: Text('Favorite'),
            ),
            body: ListView(children: divided)
        );
    }
}