import 'package:flutter/material.dart';
import 'package:webapp/view/home.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return MaterialApp(
            title: 'Jacky Flutter Learning',
            theme: ThemeData(
                primarySwatch: Colors.blue,
            ),
            home: HomePage(title: 'Jacky Tran'),
//            initialRoute: '/',
//            routes: {
//                '/': (context) => HomePage(title: 'Jacky Learning'),
//                '/favorite': (context) => FavoritePage(),
//            },
        );
    }
}
