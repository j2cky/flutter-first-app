import 'dart:convert';

import 'package:webapp/service/webservice.dart';

class Post {
    final int id;
    final String title;
    final String content;

    Post({this.id, this.title, this.content});

    factory Post.fromJson(Map<String, dynamic> json) {
        return Post(id: json['id'], title: json['title']['rendered'], content: json['content']['rendered']);
    }

    static Resource<List<Post>> get all {
        return Resource(
            url: 'https://beta.saigondeveloper.com/wp-json/wp/v2/posts',
            parse: (res) {
                final result = jsonDecode(res.body);
                Iterable list = result;
                return list.map((e) => Post.fromJson(e)).toList();
            }
        );
    }

    static Resource<Post> find(int id) {
        return Resource(
            url: 'https://beta.saigondeveloper.com/wp-json/wp/v2/posts/' + id.toString(),
            parse: (res) {
                final json = jsonDecode(res.body);
                return Post.fromJson(json);
            }
        );
    }
}